class Comment < ActiveRecord::Base
	belongs_to :post
	validates :body, presence: true, length: { minimum: 5 }
	validates :post_id, presence: true
end
